# Dvizh Api Readme

{params} - набор параметров, с которыми открывается приложение (по ним мы проверяем корректность, так что передаются везде)
---
Первый запрос, который должно отправить приложение:

`/api/frame/user/?{params}`
Ответ для администратора:
```
{
  "data": {
    "is_admin": true,
    "id": 1,
    "vk_id": 47277810,
    "vk_widget": {
      "enabled": 0,
      "events_count": 6,
      "title": "Афиша",
      "more_title": "Полный список",
      "about_title": "Подробнее",
      "widget_type": "compact_list",
      "place_prefer": "place"
    },
    "images_enabled": 1,
    "subscribers": null,
    "default_image": "user_data/1/events/default_event_5fdf02d0d5eab.png",
    "default_image_src": "http://localhost:9000/dvizh/user_data/1/events/default_event_5fdf02d0d5eab.png",
    "premium_enabled": false,
    "premium_days": 0,
    "premium_days_label": " 0 дней",
    "subscriptions_enabled": false,
    "subscribed": true
  }
}
```

По флажку `is_admin`  определяем админ это или нет и стоит ли показывать шапку сверху

По `premium_days` определяем отображать ли бейдж у кнопки премиум (если между 1 и 14, то отображаем)

`premium_days_label` текст для бейджа

По флажку `subscriptions_enabled` определяем, включена ли возможность подписаться в этом приложении и отображать ли тоггл подписки

`subscribed` - включена ли подписка на этот паблик для юзера
